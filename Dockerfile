FROM python:latest

ADD . /project
RUN pip install --upgrade -r /project/requirements.txt
RUN apt update \
    && apt install graphviz