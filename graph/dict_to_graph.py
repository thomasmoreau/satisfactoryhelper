import json
import uuid
from typing import Counter
from graphviz import Digraph


def uuid_dict(original_dict):
    """
    Fonction pour ajouter un identifiant uuid àchaque valeur du sorting system
    - Args:
        - original_dict (dict): Dictionnaire à traiter
    - Returns:
        - dict: Dictionnaire avec identifiants uuid
    """
    dict_uuid = dict()
    print()

    for key_value in original_dict.items():

        # On save les outputs dones avec un identifiant uuid unique
        if key_value[0] == "outputs_done":
            dict_uuid["outputs_done"] = list(
                map(lambda x: (f"{x} --- {uuid.uuid4()}", x), key_value[1])
            )

        # On recréé chaque stage
        else:
            dict_uuid[key_value[0]] = dict()

            # On met un uuid à chaque valeurs du stage
            dict_uuid[key_value[0]]["Values"] = list(
                map(
                    lambda x: (f"{x} --- {uuid.uuid4()}", x),
                    key_value[1]["stage_unused_list"]
                    + key_value[1]["stage_used_list"]
                    + key_value[1]["direct_outputs"],
                )
            )

            # Pour les direct outputs, on va associer avec les valeurs dans "Values"
            counter_direct_output = list()
            associated_direct_outputs = list()
            for direct_output in key_value[1]["direct_outputs"]:

                for value in dict_uuid[key_value[0]]["Values"]:

                    if (
                        value[1] == direct_output
                        and value not in associated_direct_outputs
                        and Counter(counter_direct_output)[value[1]]
                        < Counter(key_value[1]["direct_outputs"])[value[1]]
                    ):

                        counter_direct_output.append(value[1])
                        associated_direct_outputs.append(value)
            dict_uuid[key_value[0]]["Direct_Outputs"] = associated_direct_outputs

            # On met un uuid à chaque splitter
            dict_uuid[key_value[0]]["Splitters"] = list()
            for key_value_splitter in key_value[1]["splitters"].items():

                dict_uuid[key_value[0]]["Splitters"].append(
                    {
                        "Name": f"splitter--{uuid.uuid4()}",
                        "Input": key_value_splitter[1]["entry_conveyer"],
                        "Output": [
                            key_value_splitter[1]["entry_conveyer"]
                            / key_value_splitter[1]["type"]
                        ]
                        * key_value_splitter[1]["type"],
                    }
                )

            # On met un uuid à chaque merger
            dict_uuid[key_value[0]]["Mergers"] = list()
            for key_value_merger in key_value[1]["mergers"].items():

                dict_uuid[key_value[0]]["Mergers"].append(
                    {
                        "Name": f"merger--{uuid.uuid4()}",
                        "Input": key_value_merger[1]["entry_conveyers"],
                        "Output": sum(key_value_merger[1]["entry_conveyers"]),
                    }
                )

            # On met un uuid pour chaque bottleneck
            dict_uuid[key_value[0]]["Bottlenecks"] = list()
            for key_value_bottleneck in key_value[1]["bottleneck"].items():

                dict_uuid[key_value[0]]["Bottlenecks"].append(
                    {
                        "Name": f"bottleneck--{uuid.uuid4()}",
                        "Input": key_value_bottleneck[1]["entry_conveyer"],
                        "Output": key_value_bottleneck[1]["outputs"],
                        "Type": key_value_bottleneck[1]["type"],
                    }
                )

    return dict_uuid


def dict_to_graph(dict):
    dict = uuid_dict(dict)

    with open("graph/dict.json", "w") as dict_file:
        json.dump(dict, dict_file, indent=4)

    diagram = Digraph(filename="sorting_system.jpg")

    # On parcourt le dict
    for key_value in dict.items():
        # Pour chaque stage, identifié par un int
        if isinstance(key_value[0], int):
            """Values"""
            # On créé des nodes pour chaque valeurs du stage
            # Dans des sub graph pour les garder alignées
            with diagram.subgraph() as sub:
                for value in key_value[1]["Values"]:
                    sub.attr(rank="same")
                    sub.node(
                        name=value[0],
                        label=str(value[1]) + " \nStage " + str(key_value[0]),
                        shape="square",
                    )

            # Direct outputs node
            with diagram.subgraph() as sub_direct:
                sub_direct.node(name="direct_output", label="Direct Outputs")

            # Valeur utilisées par les éléments
            used_values = list()

            """Splitters"""
            # On créé une used list pour ne pas réutiliser les mêmes valeurs entre chaque splitter
            # splitters_used_values = list()

            # On itère dans la liste des splitters
            for splitter in key_value[1]["Splitters"]:
                # On créé le node
                diagram.node(
                    name=splitter["Name"],
                    label=f"Splitter - Type {len(splitter['Output'])}",
                )

                # On associe l'input
                for value in key_value[1]["Values"]:
                    if (
                        value[1] == splitter["Input"]
                        and value not in used_values
                        and value not in key_value[1]["Direct_Outputs"]
                    ):
                        associated_input = value[0]
                        used_values.append(value)
                        break

                # On associe les outputs dans le stage suivant
                associated_outputs = list()
                counter_outputs = list()
                next_stage = key_value[0] + 1
                for value_output in splitter["Output"]:

                    for check_value in dict[next_stage]["Values"]:

                        if (
                            check_value[1] == value_output
                            and check_value not in used_values
                            and Counter(counter_outputs)[check_value[1]]
                            < Counter(splitter["Output"])[check_value[1]]
                        ):
                            associated_outputs.append(check_value[0])
                            used_values.append(check_value)
                            counter_outputs.append(check_value[1])

                # On dessine les liens sur le diagram
                # L'input
                diagram.edge(associated_input, splitter["Name"])

                # Les outputs
                for output in associated_outputs:
                    diagram.edge(splitter["Name"], output)

            """Mergers"""
            # On créé une used list pour ne pas réutiliser les mêmes valeurs entre chaque merger
            # mergers_used_values = list()

            # On itère dans les mergers
            for merger in key_value[1]["Mergers"]:
                # On créé le node du merger
                diagram.node(
                    name=merger["Name"], label=f"Merger - Type {len(merger['Input'])}"
                )

                # On associe les inputs dans le stage
                associated_inputs = list()
                counter_inputs = list()
                for value_input in merger["Input"]:

                    for check_value in key_value[1]["Values"]:

                        if (
                            check_value[1] == value_input
                            and check_value not in used_values
                            and Counter(counter_inputs)[check_value[1]]
                            < Counter(merger["Input"])[check_value[1]]
                            and check_value not in key_value[1]["Direct_Outputs"]
                        ):

                            associated_inputs.append(check_value[0])
                            used_values.append(check_value)
                            counter_inputs.append(check_value[1])

                # On associe l'output dans le stage suivant
                next_stage = key_value[0] + 1
                for check_value in dict[next_stage]["Values"]:

                    if (
                        check_value[1] == merger["Output"]
                        and check_value not in used_values
                    ):

                        associated_output = check_value[0]
                        used_values.append(check_value)
                        break

                # On dessine les liens
                # Inputs
                for input in associated_inputs:
                    diagram.edge(input, merger["Name"])

                # Output
                diagram.edge(merger["Name"], associated_output)

            """Bottlenecks"""
            # On créé une used list pour ne pas réutiliser les mêmes valeurs entre chaque bottleneck
            # bottlenecks_used_values = list()

            for bottleneck in key_value[1]["Bottlenecks"]:
                # On créé le node du bottleneck
                diagram.node(
                    name=bottleneck["Name"], label=f"Bottleneck - {bottleneck['Type']}"
                )

                # On associe l'input
                for value in key_value[1]["Values"]:
                    if (
                        value[1] == bottleneck["Input"]
                        and value not in used_values
                        and value not in key_value[1]["Direct_Outputs"]
                    ):
                        associated_input = value[0]
                        used_values.append(value)
                        break

                # On associe les outputs dans le stage suivant
                associated_outputs = list()
                counter_outputs = list()
                next_stage = key_value[0] + 1

                for value_output in bottleneck["Output"]:

                    for check_value in dict[next_stage]["Values"]:

                        if (
                            check_value[1] == value_output
                            and check_value not in used_values
                            and Counter(counter_outputs)[check_value[1]]
                            < Counter(bottleneck["Output"])[check_value[1]]
                        ):

                            associated_outputs.append(check_value[0])
                            used_values.append(check_value)
                            counter_outputs.append(check_value[1])

                # On dessine les liens sur le diagram
                # L'input
                diagram.edge(associated_input, bottleneck["Name"])

                # Les outputs
                for output in associated_outputs:
                    diagram.edge(bottleneck["Name"], output)

            """Direct Outputs"""
            for direct_output in key_value[1]["Direct_Outputs"]:
                diagram.node(name=direct_output[0], label=str(direct_output[1]))
                diagram.edge(direct_output[0], "direct_output")
    #diagram.view()
