import time
import sorting_system.core as sorting_system
import graph.dict_to_graph as dict_to_graph

if __name__ == "__main__":
    start = time.perf_counter()

    # On décrit le sorting system voulu
    new_system = sorting_system.Sorter(
        max_level_conveyer=4, items_entry=None, entry_list=[480], output_list=[60] * 8
    )

    # On stock le résultat
    new_system.system_maker(0)
    print(new_system._all_stages)

    # On passe le résultat pour le transformer en diagram
    dict_to_graph.dict_to_graph(new_system._all_stages)

    finish = time.perf_counter()
    print(f"Executed in: {finish-start} seconds")
