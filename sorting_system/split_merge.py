import concurrent.futures

from itertools import combinations, combinations_with_replacement
from collections import namedtuple, Counter

# key= (stages_necessary, split_type)
# value= corresponding oparation
split_operations = {(1, 2): 1 / 2,
                    (2, 2): 1 / 4,
                    (3, 2): 1 / 8,
                    (4, 2): 1 / 16,
                    (5, 2): 1 / 32,
                    (6, 2): 1 / 64,
                    (7, 2): 1 / 128,
                    (8, 2): 1 / 256,
                    (1, 3): 1 / 3,
                    (2, 3): 1 / 9,
                    (3, 3): 1 / 27,
                    (4, 3): 1 / 81,
                    (5, 3): 1 / 243,
                    (6, 3): 1 / 729,
                    (7, 3): 1 / 2187,
                    (8, 3): 1 / 6561, }

"""All namedtuples to use"""
# NamedTuple pour nos différentes opérations
# Facilite la lecture
splitter = namedtuple(
    "Splitter",
    ["input", "target_output", "output", "type_split", "stages"]
)

merger = namedtuple(
    "Merger",
    ["input", "target_output", "type_merge", "stages"])

bottleneck = namedtuple(
    "Bottleneck",
    ["input", "target_output", "output1", "output2", "output3", "method", "stages"],
)

bottleneck_multiple = namedtuple(
    "Bottleneck_multiple",
    ["input", "target_output", "output1", "output2", "output3", "stages"])

split_to_merge = namedtuple(
    "Split_merge", ["input", "target_output", "method", "stages"])

candidate = namedtuple(
    "Candidate",
    ["output", "candidates"])


# Touver type du candidate
def find_type(obj):
    f = getattr(obj, "_fields", None)

    if f == ("input", "target_output", "output", "type_split", "stages"):
        return "splitter"

    elif f == ("input", "target_output", "type_merge", "stages"):
        return "merger"

    elif f == ("input", "target_output", "output1", "output2", "output3", "method", "stages"):
        return "bottleneck"

    elif f == ("input", "target_output", "output1", "output2", "output3", "stages"):
        return "bottleneck_multiple"

    elif f == ("input", "target_output", "method", "stages"):
        return "split_merge"

    else:
        return None


# function which return the closest value from a given value in a list
def find_closest(list, K):
    """Trouve la valeur la plus proche dans une liste
    - Args:
        - list (list): Liste dans laquelle trouver la valeur la plus proche
        - K (int/float): Valeur target
    - Returns:
        - list: Liste des valeurs les plus proches de la valeur target
    """
    return list[min(range(len(list)), key=lambda i: abs(list[i] - K))]


def is_sub_list(sub, list):
    Csub = Counter(sub)
    Clist = Counter(list)
    if all(Csub[item] <= Clist[item] for item in Csub):
        return True
    return False


def find_key(dict, value):
    """Trouve la key correspondant à une valeur donnée dans un dictionnaire
    - Args:
        - dict (dict): dictionnaire dans lequel rechercher la key
        - value (int/float): Valeur dont on veut la key
    - Returns:
        - list: Liste des keys correspondant à la valeur donnée
    """

    return list(dict.keys())[list(dict.values()).index(value)]


def split_method(input_list, output_list):
    """Fonction pour trouver la meilleure solution via splitting pour une sortie et une entrée données
    - Args:
        - conveyer_input (int/float): Entrée à splitter
        - conveyer_output (int/float): Sortie pour laquelle on doit trouver une solution
    - Returns:
        - splitter(): NamedTuple splitter() contenant la solution proposée
    """
    possibilities = []
    for conveyer_input in input_list:
        for conveyer_output in output_list:
            results = {
                operation[0]: operation[1] * conveyer_input
                for operation in split_operations.items()
            }

            closest_value = find_closest(
                list(results.values()), conveyer_output)
            type_of_split = find_key(
                results, find_closest(list(results.values()), conveyer_output)
            )[1]
            number_of_stages = find_key(
                results, find_closest(list(results.values()), conveyer_output)
            )[0]

            possibilities.append(splitter(conveyer_input,
                                          conveyer_output,
                                          closest_value,
                                          type_of_split,
                                          number_of_stages))

    return possibilities


def splitting(input_list):
    all_splitted = []

    for value in input_list:
        # Dict de la capacité de conveyer splittée
        capacities_split = {
            operation[0]: operation[1] * value for operation in split_operations.items()
        }

        # On créé une liste de toutes les inputs splittées
        for splitted_values in capacities_split.items():
            all_splitted.append(
                splitter(
                    value,
                    None,
                    splitted_values[1],
                    splitted_values[0][1],
                    splitted_values[0][0],
                )
            )
    return all_splitted


def merge_method(input_list, output_list):
    """
    Fonction pour trouver des solution via merging pour les outputs
    - Args:
        - input_list (list): Liste des inputs
        - output_list (list): Liste des output
    - Returns:
        - list: Liste de namedtuples type merger()
    """
    possibilities = []

    split_list = splitting(input_list)
    full_list = split_list + input_list

    # Groupe de 2
    for values_group in list(combinations(full_list, 2)):

        # Uniquement des int/float en entrée
        if (all(isinstance(value, (int, float)) for value in values_group)
                and is_sub_list(values_group, input_list)):
            if sum(list(values_group)) in output_list:

                possibilities.append(merger(values_group,
                                            sum(values_group),
                                            len(values_group),
                                            1))

        # Uniquement des splitters en entrée
        elif (all(find_type(value) == "splitter" for value in values_group)
                and is_sub_list([splitter.input for splitter in values_group], input_list)):

            if sum([value.output for value in values_group]) in output_list:
                possibilities.append(merger(values_group,
                                            sum([
                                                value.output for value in values_group]),
                                            len(values_group),
                                            1+max([value.stages for value in values_group])))

        # Un mix de int/float et splitters
        else:
            if (sum([value if isinstance(value, (int, float)) else value.output for value in values_group]) in output_list
                    and is_sub_list([input if isinstance(input, (int, float)) else input.input for input in values_group], input_list)):
                possibilities.append(merger(values_group,
                                            sum([value if isinstance(
                                                value, (int, float)) else value.output for value in values_group]),
                                            len(values_group),
                                            1+[value.stages for value in values_group if find_type(value) == "splitter"][0]))

    # Groupe de 3
    for values_group in list(combinations(full_list, 3)):

        # Uniquement int/float
        if (all(isinstance(value, (int, float)) for value in values_group)
                and is_sub_list(values_group, input_list)):
            if sum(list(values_group)) in output_list:
                possibilities.append(merger(values_group,
                                            sum(values_group),
                                            len(values_group),
                                            1))

        # Uniquement splitters
        elif (all(find_type(value) == "splitter" for value in values_group)
                and is_sub_list([splitter.input for splitter in values_group], input_list)):
            if sum([value.output for value in values_group]) in output_list:
                possibilities.append(merger(values_group,
                                            sum([
                                                value.output for value in values_group]),
                                            len(values_group),
                                            1 + max([value.stages for value in values_group])))

        # Un mix splitter et int/float
        else:
            if (sum([value if isinstance(value, (int, float)) else value.output for value in values_group]) in output_list
                    and is_sub_list([input if isinstance(input, (int, float)) else input.input for input in values_group], input_list)):
                possibilities.append(merger(values_group,
                                            sum([value if isinstance(
                                                value, (int, float)) else value.output for value in values_group]),
                                            len(values_group),
                                            1 + max([value.stages for value in values_group if find_type(value) == "splitter"])))

    return list(set(sorted(possibilities,
                           key=lambda x: x.stages)))


def bottleneck_method(input_list, output_list):
    """
    Fonction pour trouver des solutions via le bottleneck (contraindre le nombre d'item par le niveau du conveyer)
    - Args:
        - conveyer_input (float): Input going inside splitter
        - output_list (list): list of outputs not completed at the moment
    - Returns:
        - list: List of bottleneck named tupples
    """
    possibilities = []
    for conveyer_input in input_list:
        capacities = [60, 120, 270, 480, 780]
        all_capacities_splitted = splitting(capacities)
        # On regarde chaque valeurs dans nos capacités de conveyer
        for value in capacities:
            # Dict de la capacité de conveyer splittée
            capacities_split = {
                operation[0]: operation[1] * value for operation in split_operations.items()
            }

            # Cas 1
            # Match d'une capacities seule
            if value in output_list and conveyer_input > value:
                possibilities.append(
                    bottleneck(
                        conveyer_input,
                        value,
                        value,
                        conveyer_input - value,
                        None,
                        "Direct_Output",
                        1,
                    )
                )

            # Cas 2
            # Match d'une capacité splittée avec une output
            for value_split in capacities_split.items():
                if value_split[1] in output_list and conveyer_input > value:

                    possibilities.append(
                        bottleneck(
                            conveyer_input,
                            value_split[1],
                            value,
                            conveyer_input - value,
                            None,
                            splitter(
                                value,
                                value_split[1],
                                value_split[1],
                                value_split[0][1],
                                value_split[0][0],
                            ),
                            value_split[0][0] + 1,
                        )
                    )

        # Cas 3
        # Somme de deux capacities match
        for value_group in combinations_with_replacement(capacities, 2):
            if sum(value_group) in output_list and conveyer_input > sum(value_group):
                possibilities.append(
                    bottleneck(
                        conveyer_input,
                        sum(value_group),
                        value_group[0],
                        value_group[1],
                        conveyer_input - sum(value_group),
                        merger(value_group, sum(value_group),
                               len(value_group), 1),
                        2,
                    )
                )

        # Cas 4 -
        # 2 capacités tels que la 3eme output (y3 = conveyer_input - (y1 + y2)) match une output du système
        for value_group in combinations_with_replacement(capacities, 2):
            if (
                conveyer_input - sum(value_group) in output_list
                and conveyer_input > sum(value_group)
                and sum(value_group) != 120
            ):
                possibilities.append(
                    bottleneck(
                        conveyer_input,
                        conveyer_input - sum(value_group),
                        value_group[0],
                        value_group[1],
                        conveyer_input - sum(value_group),
                        None,
                        1,
                    )
                )

        # Cas 5
        # Somme de deux capacités splittées match
        # On créé les couples de capacités splittées
        for value_split_group in combinations_with_replacement(all_capacities_splitted, 2):
            if (
                value_split_group[0].output +
                    value_split_group[1].output in output_list
                and conveyer_input
                >= value_split_group[0].input + value_split_group[1].input
            ):
                possibilities.append(
                    bottleneck(
                        input=conveyer_input,
                        target_output=value_split_group[0].output
                        + value_split_group[1].output,
                        output1=value_split_group[0].input,
                        output2=value_split_group[1].input,
                        output3=conveyer_input
                        - (value_split_group[0].input +
                           value_split_group[1].input),
                        method=merger(
                            (value_split_group[0], value_split_group[1]),
                            value_split_group[0].output +
                            value_split_group[1].output,
                            len(value_split_group),
                            1,
                        ),
                        stages=max(
                            value_split_group[0].stages, value_split_group[1].stages)
                        + 2,
                    )
                )

    return possibilities


def bottleneck_multiple_method(input_list, output_list):
    possibilities = []
    capacities = [60, 120, 270, 480, 780]

    for output_group in combinations(output_list, 2):
        if all(value in capacities for value in output_group):
            for input in input_list:
                if input >= sum(output_group):
                    possibilities.append(
                        bottleneck_multiple(
                            input,
                            output_group,
                            output_group[0],
                            output_group[1],
                            input-sum(output_group),
                            1
                        ))

    # On peut aussi avoir un groupement de 3 si la total est égal à l'input
    for output_group in combinations(output_list, 3):
        if all(value in capacities for value in output_group):
            for input in input_list:
                if input == sum(output_group):
                    possibilities.append(
                        bottleneck_multiple(
                            input,
                            output_group,
                            output_group[0],
                            output_group[1],
                            input-sum(output_group),
                            1
                        ))

    return possibilities


def split_to_merge_method(input_list, output_list):
    """
    Fonction pour trouver si en splittant plusieurs fois une valeur, on peut retrouver une output
    - Args:
        - input_list (list): Liste des entrées
        - output_list (list): Liste des sorties
    """
    possibilities = list()

    def one_input(input):
        # Premier split
        for split_1 in splitting([input]):
            # Second split
            for split_2 in splitting([split_1.output]):
                # On check si match avec in splitter avec un merge de 2
                if split_1.output + split_2.output in output_list:
                    possibilities.append(split_to_merge(input,
                                                        split_1.output + split_2.output,
                                                        merger((split_1, split_2),
                                                               split_1.output + split_2.output,
                                                               2,
                                                               1),
                                                        max((split_1.stages,
                                                             split_2.stages))+1
                                                        ))
                else:
                    # On refait un split
                    for split_3 in splitting([split_2.output]):
                        # On check pour match avec merger de 3
                        if split_1.output + split_2.output + split_3.output in output_list:
                            possibilities.append(split_to_merge(input,
                                                                split_1.output + split_2.output + split_3.output,
                                                                merger((split_1, split_2, split_3),
                                                                       split_1.output + split_2.output + split_3.output,
                                                                       3,
                                                                       1),
                                                                max((split_1.stages,
                                                                     split_2.stages, split_3.stages))+1
                                                                ))

    # Utilisation du multithreading pour accélérer le process
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(input_list)) as executor:
        executor.map(one_input,
                     input_list)

    return possibilities


def output_possibilities_tree(input_list, output_list):
    """
    Fonction permettant de trouver toutes les possibilités pour une sortie donnée en fonction des entrées du système
    - Args:
        - input_list (list): Liste des entrées du système
        - output (int/float): Output dont on veut les possibilitées
    - Returns:
        - liste: liste des possibilités, contient splitter/merger/bottleneck
    """

    tree_possibilities = []

    # Bottleneck multiple
    try:
        tree_possibilities.extend(
            bottleneck_multiple_method(input_list, output_list))
    except:
        pass

    # Merge possibilities
    try:
        tree_possibilities.extend(merge_method(input_list, output_list))
    except:
        pass

    # Split possibilities
    try:
        tree_possibilities.extend(split_method(input_list, output_list))
    except:
        pass

    # Bottleneck
    try:
        tree_possibilities.extend(bottleneck_method(input_list, output_list))
    except:
        pass

    # Split to merge
    try:
        tree_possibilities.extend(
            split_to_merge_method(input_list, output_list))
    except:
        pass

    return sorted(tree_possibilities,
                  key=lambda x: x.stages)
