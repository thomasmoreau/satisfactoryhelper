import json
import logging

from . import split_merge

# Capacité de charge des conveyers en fonction de leur niveau
conveyers_capacity = {1: 60, 2: 120, 3: 270, 4: 480, 5: 780}


class Sorter:
    def __init__(
        self, max_level_conveyer, output_list, items_entry=None, entry_list=None
    ):
        # Verif des valeurs d'entrée
        if items_entry is None and entry_list is None:
            raise ValueError(
                "You have to give an entry list or a raw number of ressources"
            )

        elif items_entry is not None and entry_list is not None:
            raise ValueError(
                "You have to give an entry list or a raw number of ressources, not the two"
            )
        else:
            pass

        # Verif valeur du niveau max de conveyer
        if max_level_conveyer in range(1, 6):
            self._max_level_conveyer = max_level_conveyer
        else:
            raise ValueError("Conveyer level must be 1 to 5")

        # Donner capacité max des conveyers
        self._max_capacity_conveyer = conveyers_capacity[self._max_level_conveyer]

        # Verif si nombre items entrée donné
        if isinstance(items_entry, (int, float)) or items_entry is None:
            self._items_entry = items_entry
        else:
            raise ValueError("Given item entry must be int or float")

        # Arrangement des conveyers d'entrée si pas de liste donnée
        if entry_list is None:
            self._entry_list = self.sorting_entries()

        # Verifs si liste d'entrée donnée en argulent
        elif entry_list is not None and isinstance(entry_list, list):
            if all(isinstance(x, (int, float)) for x in entry_list):
                self._entry_list = entry_list
            else:
                raise ValueError(
                    "Your given entry list contains elements differents than int or float"
                )
        else:
            raise ValueError("Your given entry list is not a list")

        # Verif liste de sortie
        if isinstance(output_list, list):
            # Check type des valeurs de la liste
            if all(isinstance(x, (int, float)) for x in output_list):
                # Check si somme des outputs est inférieur ou égale à celle des inputs
                if sum(output_list) <= sum(self._entry_list):
                    self._output_list = output_list
                else:
                    raise ValueError("Items en output supérieur à l'input, impossible")
            else:
                raise ValueError("Values in your output list are not correct")

        else:
            raise ValueError("Your output given list is not correct")

        # Dictionnaire des différents stages
        # Chaque stage est identifié par un [int] représentant sa place dans le système
        self._all_stages = {"outputs_done": []}

    # Créé une liste d'entrée si un nombre d'items est donné au lieu d'une liste de conveyers
    def sorting_entries(self):
        """
        Creating list of conveyors in entry, based on max_level_conveyer
        and items_entry if no list is given

        Returns:
            [list]: [Liste des conveyers entrant dans le système ]
        """

        entry_list = [self._max_capacity_conveyer] * int(
            self._items_entry / self._max_capacity_conveyer
        )
        if self._items_entry % self._max_capacity_conveyer != 0:
            entry_list.append(self._items_entry % self._max_capacity_conveyer)
        else:
            pass
        return entry_list

    # Messages de debugging durant le process
    def debug_messages(self, current_stage, action, entry_action, target_action):
        print(
            f"Going to {action.upper()}: {self._all_stages[current_stage]}",
            f"With: {entry_action}, to match output in: {target_action}",
            f"In stage {current_stage}",
            sep="\n",
            end="\n\n",
        )

    # Création du stage avec template si stage non répertorié
    def create_stage(self, id_stage):
        if id_stage not in self._all_stages:
            self._all_stages[id_stage] = {
                "stage_unused_list": [],
                "stage_used_list": [],
                "splitters": {},
                "mergers": {},
                "bottleneck": {},
                "direct_outputs": [],
            }
            if id_stage == 0:
                self._all_stages[id_stage]["stage_unused_list"] = self._entry_list
        else:
            pass

    # Ajouter info d'un splitter dans le stage en cours
    def add_splitter_infos(self, type_splitter, entry_conveyer, current_stage):
        if self._all_stages[current_stage]["splitters"]:
            id_splitter = (
                list(self._all_stages[current_stage]["splitters"].keys())[-1] + 1
            )
        else:
            id_splitter = 1
        self._all_stages[current_stage]["splitters"][id_splitter] = {
            "type": type_splitter,
            "entry_conveyer": entry_conveyer,
        }

    # Ajouter infos bottleneck
    def add_bottleneck_infos(
        self, type_bottleneck, entry_conveyer, outputs, current_stage
    ):
        if self._all_stages[current_stage]["bottleneck"]:
            id_bottleneck = (
                list(self._all_stages[current_stage]["bottleneck"].keys())[-1] + 1
            )
        else:
            id_bottleneck = 1

        self._all_stages[current_stage]["bottleneck"][id_bottleneck] = {
            "type": type_bottleneck,
            "entry_conveyer": entry_conveyer,
            "outputs": outputs,
        }

    # Ajouter infos d'un merger dans le stage en cours
    def add_merger_infos(self, type_merger, conveyer_input_list, current_stage):
        if self._all_stages[current_stage]["mergers"]:
            id_merger = list(self._all_stages[current_stage]["mergers"].keys())[-1] + 1
        else:
            id_merger = 1
        self._all_stages[current_stage]["mergers"][id_merger] = {
            "type": type_merger,
            "entry_conveyers": conveyer_input_list,
        }

    # Traitement pour les direct outputs
    def direct_output_action(self, current_stage):
        # On créé une copie de nos inputs car itérer dans une liste et en supprimer des éléments empêche de parcourir tous les éléments
        # On itère donc autour de la copie et on supprime les valeurs dans la vraie liste du stage
        input_list = self._all_stages[current_stage]["stage_unused_list"][:]

        for value in input_list:
            # Debug message
            self.debug_messages(
                current_stage, "check for direct outputs", value, self._output_list
            )

            if value in self._output_list:
                # Message debug
                self.debug_messages(
                    current_stage,
                    "direct output",
                    value,
                    f"{value} in {self._output_list}",
                )

                # Retirer output de la liste des outputs à produire
                self._output_list.remove(value)

                # Retirer input des entrées non utilisées
                self._all_stages[current_stage]["stage_unused_list"].remove(value)

                # Ajouter à la liste des directs outputs du stage
                self._all_stages[current_stage]["direct_outputs"].append(value)

                # Ajouter à la liste des sorties réussies
                self._all_stages["outputs_done"].append(value)

    # Traitement pour les mergers
    def merger_actions(self, current_stage, merger):
        # Message debug
        self.debug_messages(current_stage, "merge", merger, self._output_list)

        # Cas 1 - Toutes les inputs sont des (int, float)
        if all(isinstance(input, (int, float)) for input in merger.input):
            # Retirer les inputs de la liste de unused du stage
            [
                self._all_stages[current_stage]["stage_unused_list"].remove(input)
                for input in merger.input
            ]

            # Ajouter les inputs au used du stage
            self._all_stages[current_stage]["stage_used_list"].extend(merger.input)

            # Ajouter infos du merger dans le stage en cours
            self.add_merger_infos(merger.type_merge, merger.input, current_stage)

            # Merge n'a un impact que sur le stage suivant
            # On s'assure que le stage suivant existe
            self.create_stage(current_stage + 1)

            # Ajouter aux direct outputs du stage final
            self._all_stages[current_stage + merger.stages]["direct_outputs"].append(
                merger.target_output
            )

            self._all_stages["outputs_done"].append(merger.target_output)
            self._output_list.remove(merger.target_output)

        # Cas 2 - Toutes les inputs sont des splitters
        elif all(split_merge.find_type(input) == "splitter" for input in merger.input):

            # Ajouter les infos des splitters
            # Va également supprimer les unused et ajouter les used
            # Le merger des valeurs finales sera pris en compte par le possibilities tree
            [self.splitter_action(current_stage, input) for input in merger.input]

            """# Ajouter infos du merger dans le stage final
            self.add_merger_infos(
                merger.type_merge,
                [splitter.output for splitter in merger.input],
                current_stage + merger.stages-1)

            # Ajouter les inputs au used du stage
            self._all_stages[current_stage + merger.stages-1]["stage_used_list"].extend(
                [splitter.output for splitter in merger.input])

            # Ajouter aux direct outputs du stage final
            self._all_stages[current_stage +
                             merger.stages-1]["direct_outputs"].append(merger.target_output)"""

        # Cas 3 des inputs mixées
        else:
            # Splitter actions si input= splitter()
            [
                self.splitter_action(current_stage, input)
                for input in merger.input
                if split_merge.find_type(input) == "splitter"
            ]

            # Retirer les inputs de la liste de unused du stage si input type int, float
            # Sinon splitter_action() l'aura fait dans l'étape précédente si input est splitter
            [
                self._all_stages[current_stage]["stage_unused_list"].remove(value)
                for value in merger.input
                if isinstance(value, (int, float))
            ]

            # Ajouter les inputs au used du stage si input type int, float
            [
                self._all_stages[current_stage]["stage_used_list"].append(input)
                for input in merger.input
                if isinstance(input, (int, float))
            ]

            # Ajouter infos du merger dans le stage final
            """self.add_merger_infos(
                merger.type_merge,
                [merger_input if isinstance(
                    merger_input, (int, float)) else merger_input.output for merger_input in merger.input],
                current_stage+merger.stages-1)

            # Ajouter aux direct outputs du stage final
            self._all_stages[current_stage +
                             merger.stages-1]["direct_outputs"].append(merger.target_output)"""

    # Traitement pour les splitters
    def splitter_action(self, _current_stage, splitter):
        # Message debug
        self.debug_messages(_current_stage, "split", splitter, self._output_list)

        # Retirer input des unused values
        self._all_stages[_current_stage]["stage_unused_list"].remove(splitter.input)

        # Ajouter input aux used values
        self._all_stages[_current_stage]["stage_used_list"].append(splitter.input)

        # Ajouter infos du splitter dans le stage en cours
        self.add_splitter_infos(splitter.type_split, splitter.input, _current_stage)

        # Ajouter les input au stages intermédiaires nécessaires au splitting
        for stage in range(1, splitter.stages + 1):
            # On s'assure que le stage existe
            self.create_stage(_current_stage + stage)

            # Stages intermédiaires de l'opération
            """2 stages ou plus"""
            # Si on a un split par 2
            if splitter.type_split == 2 and stage != splitter.stages:
                # On ajoute un sortie du plitter aux used
                self._all_stages[_current_stage + stage]["stage_used_list"].append(
                    (1 / 2 ** stage) * splitter.input
                )

                # On ajoute un sortie du plitter aux unused
                self._all_stages[_current_stage + stage]["stage_unused_list"].append(
                    (1 / 2 ** stage) * splitter.input
                )

                # On ajoute les infos du splitter sur le stage concerné
                self.add_splitter_infos(
                    2, (1 / 2 ** stage) * splitter.input, _current_stage + stage
                )

            # Si on a un split par 3
            elif splitter.type_split == 3 and stage != splitter.stages:
                # On ajoute une sortie du plitter aux used
                self._all_stages[_current_stage + stage]["stage_used_list"].append(
                    (1 / 3 ** stage) * splitter.input
                )

                # On ajoute deux sorties du plitter aux unused
                self._all_stages[_current_stage + stage]["stage_unused_list"].extend(
                    [(1 / 3 ** stage) * splitter.input] * 2
                )

                # On ajoute les infos du splitter sur le stage concerné
                self.add_splitter_infos(
                    3, (1 / 3 ** stage) * splitter.input, _current_stage + stage
                )

            # Dernier stage de l'opération
            elif splitter.type_split == 2 and stage == splitter.stages:
                # On ajoute une sortie aux unused
                self._all_stages[_current_stage + stage]["stage_unused_list"].append(
                    (1 / 2 ** stage) * splitter.input
                )

                # On ajoute une sortie aux direct outputs si ça doit être fait
                # Sinon aux unused
                if (1 / 2 ** stage) * splitter.input in self._output_list:
                    # On ajoute aux direct outputs du stage
                    self._all_stages[_current_stage + stage]["direct_outputs"].append(
                        (1 / 2 ** stage) * splitter.input
                    )

                    # Retirer output de la liste des outputs à produire
                    self._output_list.remove(splitter.target_output)

                    # On ajoute la sortie aux outputs done du système
                    self._all_stages["outputs_done"].append(
                        (1 / 2 ** stage) * splitter.input
                    )
                else:
                    self._all_stages[_current_stage + stage][
                        "stage_unused_list"
                    ].append((1 / 2 ** stage) * splitter.input)

            elif splitter.type_split == 3 and stage == splitter.stages:
                # On ajoute deux sorties aux unused
                self._all_stages[_current_stage + stage]["stage_unused_list"].extend(
                    [(1 / 3 ** stage) * splitter.input] * 2
                )

                # On ajoute une sortie aux direct outputs si ça doit être fait
                if (1 / 3 ** stage) * splitter.input in self._output_list:
                    # On ajoute aux direct outputs du stage
                    self._all_stages[_current_stage + stage]["direct_outputs"].append(
                        (1 / 3 ** stage) * splitter.input
                    )

                    # Retirer output de la liste des outputs à produire
                    self._output_list.remove(splitter.target_output)

                    # On ajoute la sortie aux outputs done du système
                    self._all_stages["outputs_done"].append(
                        (1 / 3 ** stage) * splitter.input
                    )
                else:
                    self._all_stages[_current_stage + stage][
                        "stage_unused_list"
                    ].append((1 / 3 ** stage) * splitter.input)

            # Si aucune action possible, error
            else:
                raise KeyError("Impossible to create intermediate stage or final stage")

    # Traitement pour les bottleneck
    def bottleneck_actions(self, current_stage, bottleneck):
        # Message debug
        self.debug_messages(current_stage, "bottleneck", bottleneck, self._output_list)

        # Retirer l'input de la liste de unused du stage
        self._all_stages[current_stage]["stage_unused_list"].remove(bottleneck.input)

        # Ajouter les input au used du stage
        self._all_stages[current_stage]["stage_used_list"].append(bottleneck.input)

        # Selon la method du bottleneck, on réalise différentes opérations
        """
        Cas 1 - Si direct output
        """
        if bottleneck.method == "Direct_Output":
            # Ajouter infos du bottleneck dans le stage en cours
            self.add_bottleneck_infos(
                "Direct Output",
                bottleneck.input,
                (bottleneck.output1, bottleneck.output2, bottleneck.output3),
                current_stage,
            )

            # Retirer output de la liste des outputs à produire
            self._output_list.remove(bottleneck.target_output)

            # On s'assure stage suivant existe
            self.create_stage(current_stage + 1)

            # On ajoute la sortie aux direct outputs
            self._all_stages[current_stage + 1]["direct_outputs"].append(
                bottleneck.output1
            )

            # On ajoute l'autre sortie au unused values
            self._all_stages[current_stage + 1]["stage_unused_list"].append(
                bottleneck.output2
            )

            # On ajoute aux sorties réussies
            self._all_stages["outputs_done"].append(bottleneck.output1)

            return None

        """
        Cas 2 - Match d'une capacité splittée avec une output
        """
        if split_merge.find_type(bottleneck.method) == "splitter":
            # Ajouter infos du bottleneck dans le stage en cours
            self.add_bottleneck_infos(
                "Splitted Bottleneck",
                bottleneck.input,
                (bottleneck.output1, bottleneck.output2, bottleneck.output3),
                current_stage,
            )

            # On s'assure stage suivant existe
            self.create_stage(current_stage + 1)

            # On ajoute les outputs aux unused du stage suivant avant les actions
            for output in [bottleneck.output1, bottleneck.output2, bottleneck.output3]:
                if isinstance(output, (float, int)) and output > 0:
                    self._all_stages[current_stage + 1]["stage_unused_list"].append(
                        output
                    )

            # On effectue les actions pour le stage suivant
            self.splitter_action(current_stage + 1, bottleneck.method)

            return None

        """
        Cas 3 - Somme de deux capacities match
        """
        if split_merge.find_type(bottleneck.method) == "merger":
            # Ajouter infos du bottleneck dans le stage en cours
            self.add_bottleneck_infos(
                "Merger",
                bottleneck.input,
                (bottleneck.output1, bottleneck.output2, bottleneck.output3),
                current_stage,
            )

            # Check pour ne pas confondre avec la cas 5
            if (
                split_merge.find_type(bottleneck.method.input[0]) != "splitter"
                and split_merge.find_type(bottleneck.method.input[1]) != "splitter"
            ):

                # On s'assure stage suivant existe
                self.create_stage(current_stage + 1)

                # On ajoute les outputs aux unused du stage suivant avant les actions
                for output in [
                    bottleneck.output1,
                    bottleneck.output2,
                    bottleneck.output3,
                ]:
                    if isinstance(output, (float, int)) and output > 0:
                        self._all_stages[current_stage + 1]["stage_unused_list"].append(
                            output
                        )

                # On effectue les actions pour le stage suivant
                self.merger_actions(current_stage + 1, bottleneck.method)

                return None

        """
        Cas 4 - 2 capacités tels que la 3eme output (y3 = conveyer_input - (y1 + y2)) match une output du système
        """
        if bottleneck.method == None:
            # Ajouter infos du bottleneck dans le stage en cours
            self.add_bottleneck_infos(
                "Third Output",
                bottleneck.input,
                (bottleneck.output1, bottleneck.output2, bottleneck.output3),
                current_stage,
            )

            # On s'assure stage suivant existe
            self.create_stage(current_stage + 1)

            # On effectue les actions sur le stage suivant
            # On ajoute les sorties récupérées au stage suivant
            for output in [bottleneck.output1, bottleneck.output2, bottleneck.output3]:
                # Verif si aucunes valeur à None
                if isinstance(output, (float, int)) and output > 0:
                    self._all_stages[current_stage + 1]["stage_unused_list"].append(
                        output
                    )

            return None

        """
        Cas 5 - Somme de deux capacités splittées match une output
        """
        if split_merge.find_type(bottleneck.method) == "merger":
            # On a déja un cas ou la méthode est un merger, on check pour ce cas si les inputs sont des splitters
            if all(
                split_merge.find_type(input) == "splitter"
                for input in bottleneck.method.input
            ):

                # Ajouter infos du bottleneck dans le stage en cours
                self.add_bottleneck_infos(
                    "Merge splitted",
                    bottleneck.input,
                    (bottleneck.output1, bottleneck.output2, bottleneck.output3),
                    current_stage,
                )

                # On s'assure stage suivant existe
                self.create_stage(current_stage + 1)

                # On ajoute les outputs aux unused du stage suivant avant les actions
                for output in [
                    bottleneck.output1,
                    bottleneck.output2,
                    bottleneck.output3,
                ]:
                    if isinstance(output, (float, int)) and output > 0:
                        self._all_stages[current_stage + 1]["stage_unused_list"].append(
                            output
                        )

                # On effectue les action pour chaque splitter dans le merger
                for splitter in bottleneck.method.input:
                    self.splitter_action(current_stage + 1, splitter)

            return None

    # Traitement pour les bottleneck à sorties multiples
    def bottleneck_multiple_action(self, current_stage, bottleneck_multiple):
        # Message debug
        self.debug_messages(
            current_stage, "bottleneck-multiple", bottleneck_multiple, self._output_list
        )

        # On ajoute les infos du bottleneck
        self.add_bottleneck_infos(
            "Multiple",
            bottleneck_multiple.input,
            (
                bottleneck_multiple.output1,
                bottleneck_multiple.output2,
                bottleneck_multiple.output3,
            ),
            current_stage,
        )

        # On retire l'input des unused du stage
        self._all_stages[current_stage]["stage_unused_list"].remove(
            bottleneck_multiple.input
        )

        # On ajoute aux used du stage
        self._all_stages[current_stage]["stage_used_list"].append(
            bottleneck_multiple.input
        )

        # On retire les outputs de la todo list
        [
            self._output_list.remove(output)
            for output in bottleneck_multiple.target_output
        ]

        # On s'assure que le stage suivant existe
        self.create_stage(current_stage + 1)

        # On ajoute simplement les outputs trouvées aux direct outputs du stage suivant
        self._all_stages[current_stage + 1]["direct_outputs"].extend(
            bottleneck_multiple.target_output
        )

        # On ajoute la 3eme output au unuseddu stage suivant
        self._all_stages[current_stage + 1]["stage_unused_list"].append(
            bottleneck_multiple.output3
        )

    # Traitement pour les pour les splitters to mergers
    def split_to_merge_action(self, current_stage, split_merge):

        # On fait les actions requises
        self.splitter_action(current_stage, split_merge.method.input[0])
        self.splitter_action(
            current_stage + split_merge.method.input[0].stages,
            split_merge.method.input[1],
        )

    # Gestion des action en fonction du type de candidate donné
    def action_by_type(self, current_stage, candidate):
        # On check le type du candidate
        CandidateType = split_merge.find_type(candidate)

        # Debug message
        print(f"Choose candidate {candidate}", end="\n\n")

        # On fait les actions nécessaires selon le type
        if CandidateType == "splitter":
            self.splitter_action(current_stage, candidate)
        elif CandidateType == "merger":
            self.merger_actions(current_stage, candidate)
        elif CandidateType == "bottleneck":
            self.bottleneck_actions(current_stage, candidate)
        elif CandidateType == "bottleneck_multiple":
            self.bottleneck_multiple_action(current_stage, candidate)
        else:
            self.split_to_merge_action(current_stage, candidate)

    # ToDo - Milestone Gitlab
    def candidate_management(self, current_stage, possibilities):
        # Tri des candidats viables
        for candidate in possibilities:

            """Mergers"""
            if split_merge.find_type(candidate) == "merger":
                if (
                    all(
                        input in self._all_stages[current_stage]["stage_unused_list"]
                        for input in [
                            entry if isinstance(entry, (int, float)) else entry.input
                            for entry in candidate.input
                        ]
                    )
                    and candidate.target_output in self._output_list
                ):

                    self.action_by_type(current_stage, candidate)

            """Splitters"""
            if split_merge.find_type(candidate) == "splitter":
                if (
                    candidate.output == candidate.target_output
                    and candidate.input
                    in self._all_stages[current_stage]["stage_unused_list"]
                    and candidate.target_output in self._output_list
                ):

                    self.action_by_type(current_stage, candidate)

            """Bottlenecks"""
            if split_merge.find_type(candidate) == "bottleneck":
                if (
                    candidate.input
                    in self._all_stages[current_stage]["stage_unused_list"]
                    and candidate.target_output in self._output_list
                ):

                    self.action_by_type(current_stage, candidate)

            """Bottlenck multiples"""
            if split_merge.find_type(candidate) == "bottleneck_multiple":
                if candidate.input in self._all_stages[current_stage][
                    "stage_unused_list"
                ] and all(
                    output in self._output_list for output in candidate.target_output
                ):

                    self.action_by_type(current_stage, candidate)

            """Splitter to merger"""
            if split_merge.find_type(candidate) == "split_merge":
                if (
                    candidate.input
                    in self._all_stages[current_stage]["stage_unused_list"]
                    and candidate.target_output in self._output_list
                ):

                    self.action_by_type(current_stage, candidate)

    # Créations des stages pour obtenir résultat voulu
    def system_maker(self, current_stage):
        """
        Préparation du stage
        """

        # Création stage si n'existe pas dans les stage du système
        # Si c'est le premier stage, la fonction lui donne la liste d'entrée du system
        self.create_stage(current_stage)

        # Logging infos of the stage
        self.debug_messages(
            current_stage,
            "start: " + str(current_stage) + "-" * 45,
            f"Output list: {self._output_list}",
            "",
        )

        """
        Actions ordered from less efforts to much efforts
        1- Test si direct_outputs possibles
        2- Make possibilities tree
        3- Choose best candidate for each output
        4- Faire les actions qui découlent, selon le type de traitement choisi
        5- Finir traitement du stage en cours
        """
        # Création de candidates
        possibilities_tree = split_merge.output_possibilities_tree(
            self._all_stages[current_stage]["stage_unused_list"], self._output_list
        )

        # Affichage des candidates
        print(possibilities_tree)

        # Direct outputs
        self.direct_output_action(current_stage)

        # Actions pour les candidats
        self.candidate_management(current_stage, possibilities_tree)

        # On passe les unused du stage actuel au stage suivant
        try:
            [
                self._all_stages[current_stage + 1]["stage_unused_list"].append(value)
                for value in self._all_stages[current_stage]["stage_unused_list"]
            ]
        except:
            pass

        if current_stage == list(self._all_stages.keys())[-1]:
            return "Done"

        else:
            self.system_maker(current_stage + 1)
